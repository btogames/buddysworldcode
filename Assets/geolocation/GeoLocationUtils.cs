﻿using UnityEngine;
using System.Collections;

static public class GeoLocationUtils {

	static public bool isNearMeKilometers(Location me, Location thing, float maxDistance = 0.2f) {
		float distanceToThing = getDistanceFromLatLonInKm (me.lat, me.lng, thing.lat, thing.lng);
		return distanceToThing <= maxDistance;
	}

	static public bool isNearMeMeters(Location me, Location thing, float maxMetersDistante = 100f) {
		float distanceFromThingInMeters = getDistanceBetweenPointsInMeters(me, thing);
		return distanceFromThingInMeters <= maxMetersDistante;
	}

	static public bool isNearMeMetersRound(Location me, Location thing, int maxMetersDistante = 20) {
		int distanceFromThingInMeters = getDistanceBetweenPointsInMetersRound(me, thing);
		return distanceFromThingInMeters <= maxMetersDistante;
	}

	static public Vector3 getPositionOfObjectFromMe(Location me, Location thing, float multiplier = 0.2f) {
		

		int distanceToThingInMeters = getDistanceBetweenPointsInMetersRound(me, thing);

		var objectDirection = new Vector3 {
			x = me.lat - thing.lat,
			y = 0.0f,
			z = me.lng - thing.lng
		};

		objectDirection = Vector3.Normalize(objectDirection);
		objectDirection.x *= distanceToThingInMeters;
		objectDirection.z *= distanceToThingInMeters;

		return objectDirection;
	}

	static public float getDistanceBetweenPointsInMeters(Location p1, Location p2) {
		return 1000 * GeoLocationUtils.getDistanceFromLatLonInKm(p1.lat, p1.lng, p2.lat, p2.lng); 
	}

	static public int getDistanceBetweenPointsInMetersRound(Location p1, Location p2) {
		return Mathf.RoundToInt(1000 * GeoLocationUtils.getDistanceFromLatLonInKm(p1.lat, p1.lng, p2.lat, p2.lng)); 
	}

	static public float getDistanceFromLatLonInKm(float lat1, float lon1, float lat2, float lon2) {
		var R = 6371; // Radius of the earth in km
		var dLat = deg2rad(lat2-lat1);  // deg2rad below
		var dLon = deg2rad(lon2-lon1); 
		var a =
			Mathf.Sin(dLat/2) * Mathf.Sin(dLat/2) +
			Mathf.Cos(deg2rad(lat1)) * Mathf.Cos(deg2rad(lat2)) * 
			Mathf.Sin(dLon/2) * Mathf.Sin(dLon/2); 
		var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1-a)); 
		var d = R * c; // Distance in km
		return d;
	}

	static float deg2rad(float deg) {
		return deg * (Mathf.PI/180);
	}
}
