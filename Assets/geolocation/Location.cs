﻿using System;

[Serializable]
public struct Location
{
	public float lat;
	public float lng;
}

