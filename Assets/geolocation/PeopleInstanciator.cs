﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PeopleInstanciator : MonoBehaviour {

	public const int UsainBoltMaxVelocityInMetersPerSecond = 13; // 
	public float maxDistance = 100.0f;
	public float distanceMultiplier = 1.0f;
	public float updateRatio = 1.0f;
	public Location mockSet = new Location ();
	public static PersonObject myObject {get; private set;}
	AccessToken currentAccessToken;
	PersonApiController pac = new PersonApiController();
	PeopleLocalizator pl;
	List<PersonObject> peopleObjects = new List<PersonObject>();
	HeadsUpDisplay hud;
	GPSReceiver receiver;
	static Person nearestPerson = null;
	static int nearestPersonDistance = 0;
	// Use this for initialization

	static public Person getNearest() {
		return PeopleInstanciator.nearestPerson;
	}

	void Awake() {
		hud = GameObject.Find("HUD").GetComponent<HeadsUpDisplay>();
		receiver = gameObject.AddOrGetComponent<GPSReceiver>();
	}
	void Start () {
		receiver.turnOn();

		pl = PeopleLocalizator.create ();
		currentAccessToken = PersonApiController.currentAccessToken;
		Debug.Log("AccessToken: " + currentAccessToken.id);
		pac.onGETme += setupMeAndUpdateHandlers;
		pac.getMe (currentAccessToken);
	}

	void setupMeAndUpdateHandlers(Person me) {
		
		me.accessToken = currentAccessToken.id;

		myObject = PersonObject.createFrom (me);
		myObject.name = me.name == null || me.name.Equals("") ? "Me" : me.name;

		//remove my box collider
		myObject.GetComponentInChildren<BoxCollider>().enabled = false;

		receiver.mockLocation = me.location;
		receiver.addOnUpdateAction(onGPSCoordinatesUpdate);
		

		getMyCurrentConfigAndSendItToServer();
		InvokeRepeating ("updatePeopleAround", 1, 1);
		InvokeRepeating ("getMyCurrentConfigAndSendItToServer", 1, 1);
	}

	void onGPSCoordinatesUpdate(Location received) {
		myObject.me.lastLocation = myObject.me.location;
		myObject.me.location = received;
	}
	
	void updatePeopleAround() {
		pl.maxDistance = maxDistance;
		pl.getWhoIsAroundMyLocationAndSetProcessingFunction (myObject.me.location, instantiateObjectsWithPeopleConfig);
	}

	void getMyCurrentConfigAndSendItToServer() {
		
		playRunAnimationIfDistanceFromLastPositionIsHigherElsePlayIdle(myObject);
		pac.updateMe (currentAccessToken, myObject.me);
	}

	void instantiateObjectsWithPeopleConfig(Person[] people) {
		if (people != null) {
			for (int i = 0; i < people.Length; i++) {
				Person person = people [i];
				if (isNotMe (person)) {

					int distance = GeoLocationUtils.getDistanceBetweenPointsInMetersRound(myObject.me.location, person.location);
					
					if (distance < nearestPersonDistance || nearestPersonDistance == 0) {
						nearestPerson = person;
						nearestPersonDistance = distance;
					} 

					PersonObject po = findByIdOrCreateAndPushToArray (person);
					tryToUpdatePersonObjectWithPersonConfig (po, person);
					tryUpdatePositionAndRotationOfPersonObject (po);
					playRunAnimationIfDistanceFromLastPositionIsHigherElsePlayIdle (po);
				}
			}
		}

		tryClearWhoIsNotAroundAnyMore ();
	}

	bool isNotMe(Person other) {
		return !myObject.me.id.Equals (other.id);
	}

	PersonObject findByIdOrCreateAndPushToArray(Person person) {
		PersonObject po = findPersonById (person.id);

		if (po == null) {
			po = PersonObject.createFrom (person);
			po.name = person.id;
			peopleObjects.Add(po);
		}

		return po;
	}

	void tryToUpdatePersonObjectWithPersonConfig(PersonObject po, Person person) {
		po.setPersonConfig (person);
	}

	void tryUpdatePositionAndRotationOfPersonObject(PersonObject po) {
		Vector3 newPosition = GeoLocationUtils.getPositionOfObjectFromMe (myObject.me.location, po.me.location, distanceMultiplier);
		Vector3 lastPosition = GeoLocationUtils.getPositionOfObjectFromMe (myObject.me.lastLocation, po.me.lastLocation, distanceMultiplier);
		po.updatePosition (newPosition);
		po.updateRotationDirection (lastPosition, newPosition);
		// TODO: Update other people rotation is a 2 step rotation
		//	1º - rotation around theyself
		//	2º - rotation around the player
	}

	PersonObject findPersonById(string id) {
		
		for (int i = 0; i < peopleObjects.Count; i++) {
			PersonObject po = peopleObjects [i];
			if (po.me.id == id) return po;
		}

		return null;
	}

	void tryClearWhoIsNotAroundAnyMore() {

		List<PersonObject> peopleToRemove = new List<PersonObject> ();

		for (int i = 0; i < peopleObjects.Count; i++) {
			PersonObject po = peopleObjects [i];
			bool isDistanteFromMe = !myObject.me.isNear (po.me, maxDistance);
			if (isDistanteFromMe) {
				Debug.Log(po.me.id + " is not near anymore...");
				peopleToRemove.Add (po);
				GameObject.Destroy (po.gameObject);
			}
		}

		foreach(PersonObject po in peopleToRemove){
			peopleObjects.Remove (po);
		}
	}


	void playRunAnimationIfDistanceFromLastPositionIsHigherElsePlayIdle(PersonObject po) {
		
		int distanceFromLastLocation = GeoLocationUtils.getDistanceBetweenPointsInMetersRound (po.me.location, po.me.lastLocation);
		
		if (distanceFromLastLocation > 1) {

			// only add path if the last run second was faster at maximum to usain bolt speed
			if (distanceFromLastLocation <= UsainBoltMaxVelocityInMetersPerSecond) {
				myObject.me.metersRan += distanceFromLastLocation;
				Debug.Log("Running " + distanceFromLastLocation + " meters");
			}
			po.playRunAnimation ();
		} 
		else {
			po.playIdleAnimation ();
		}
	}

	[ContextMenu("Show Who is on list")]
	void showWhoIsOnList() {
		for (int i = 0; i < peopleObjects.Count; i++) {
			PersonObject po = peopleObjects [i];
			float distanceFromMe = GeoLocationUtils.getDistanceBetweenPointsInMeters(myObject.me.location, po.me.location);
			Debug.Log(po.me.id + " is " + distanceFromMe + " meters dist");
		}
	}

}
