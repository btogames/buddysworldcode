﻿using UnityEngine;
using UnityEngine.Events;
using Newtonsoft.Json;

public class PeopleLocalizator : MonoBehaviour
{
	

	public float maxDistance = 1000.0f;

	static public PeopleLocalizator create() {
		return new GameObject ("PeopleLocalizator").AddOrGetComponent<PeopleLocalizator>();
	}

	public void getWhoIsAroundMyLocationAndSetProcessingFunction(Location location, UnityAction<Person[]> processPeopleAround) {

		string urlParams = "around?lat=" + location.lat + "&lng=" + location.lng + "&maxDistance=" + maxDistance + "&unit=meters";
		string fullServerURI = ServerConsts.PERSON_API_URL + urlParams;

		Requester.getFromAndProcessWith (fullServerURI, (jsonStringBody) => transformJsonStringToPersonArray(jsonStringBody, processPeopleAround));
	}

	void transformJsonToPersonConfig(string jsonString, UnityAction<Person> processMyUpdate) {		
		processMyUpdate(JsonConvert.DeserializeObject <Person>(jsonString));
	}

	void transformJsonStringToPersonArray(string jsonString, UnityAction<Person[]> processPeopleAround) {
		Person[] people = JsonArray.fromJsonArray<Person> (jsonString);
		processPeopleAround (people);
	}
}

