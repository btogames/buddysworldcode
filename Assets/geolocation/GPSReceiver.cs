using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;


public class UpdateCoordinatesEvent : UnityEvent<Location> {

}

public class GPSReceiver : MonoBehaviour {

    static bool isGPSReady = false;
    bool isGPSEnabled = false;
    float updateRatio = 1f;
    int maxWaitInSeconds = 10;
    Coroutine gpsUpdateCoroutine = null;

    UpdateCoordinatesEvent onUpdateCoordinates = new UpdateCoordinatesEvent();

    public Location mockLocation = new Location();

    public void addOnUpdateAction(UnityAction<Location> onUpdateAction) {
        this.onUpdateCoordinates.AddListener(onUpdateAction);
    }


    public void turnOn() {
       gpsUpdateCoroutine = StartCoroutine(requestGPSCoordinatesRepeating());
    }

    public void turnOff() {
        if (gpsUpdateCoroutine != null) {
            StopCoroutine(gpsUpdateCoroutine);
            gpsUpdateCoroutine = null;
        }
        isGPSReady = false;
        this.isGPSEnabled = false;
    }

    public void setMaxWaitServiceForServiceStart(int maxWaitInSeconds) {
        this.maxWaitInSeconds = maxWaitInSeconds;
    }

    public void setUpdateRatio(float seconds) {
        this.updateRatio = seconds;
    }

    public bool isReady() {
        return isGPSReady;
    }

    IEnumerator requestGPSCoordinatesRepeating() {

        if (!isGPSReady) {
            this.isGPSEnabled = Input.location.isEnabledByUser;
            if (!this.isGPSEnabled) {
                Debug.Log ("Location Service is not Enabled");
            }
            else {
                Input.location.Start (0.1f, 0.1f);
                int countDownSecondsToStart = this.maxWaitInSeconds;

                if (Input.location.status == LocationServiceStatus.Initializing && countDownSecondsToStart > 0) {
                    Debug.Log ("Initializing GPS...");
                    countDownSecondsToStart--;
                    yield return new WaitForSeconds (updateRatio);
                }

                if (Input.location.status == LocationServiceStatus.Failed || Input.location.status == LocationServiceStatus.Stopped) {
                    Debug.Log("Service not started");
                } 
                else {
                    isGPSReady = true;
                }
            }
        }

		while (true) {

            Location received = new Location();

            #if UNITY_EDITOR
            received = mockLocation;
            #else
            received.lat = Input.location.lastData.latitude;
            received.lng = Input.location.lastData.longitude;
            #endif

            if (this.onUpdateCoordinates != null) {
                this.onUpdateCoordinates.Invoke(received);
            }

			yield return new WaitForSeconds(updateRatio);
		}
	}
}