﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConfigUI : MonoBehaviour {

	public InputField configName;
	private CanvasGroup canvasGroup;

	void Awake() {
		this.canvasGroup = gameObject.GetComponent<CanvasGroup>();
		StartCoroutine(setConfigReceivedFromServer());
		this.configName.onValueChange.AddListener(onChangeName);
	}



	public void toggle(bool shouldShow) {
		this.canvasGroup.alpha = shouldShow ? 1f : 0f;
		this.canvasGroup.interactable = shouldShow;
		this.canvasGroup.blocksRaycasts = shouldShow;
	}

	private void onChangeName(string newName) {
		PeopleInstanciator.myObject.me.name = newName;
	}

	private IEnumerator setConfigReceivedFromServer() {

		while(PeopleInstanciator.myObject == null) {
			yield return null;
		}

		this.configName.text = PeopleInstanciator.myObject.me.name != null ? PeopleInstanciator.myObject.me.name : "";

		yield return null;
	}
}
