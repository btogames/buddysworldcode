﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RunningChallengeUI : MouseCapture {

	const string CHALLENGE_TEXT = "Challenge <NAME> to run?";
	const string METERS_RAN = "<DIST> meters ran!";
	const string ACCEPTANCE_TEXT = "<NAME> is challenging you to run <DIST> m. Do you accept?";

	public CanvasGroup challengePanel;
	public Slider meterSlider;
	public Text meterText;
	public int meters = 0;
	public Text challengedName;
	public Button inviteButton;
	public Button cancelButton;

	public Text metersCounter;
	public Text runStatus;
	private string currentStatus;
	private ChallengeApiController cac;

	private Challenge currentChallenge;
	private Person lastSelectedPerson;

	public CanvasGroup acceptanceMenu;
	public Button acceptChallengeButton;
	public Button rejectChallengeButton;
	public Text acceptanceText;

	public bool waitingForServerToResponde = false;
	void Awake() {
		cac = new ChallengeApiController();
		StartCapture();
		this.meterSlider.onValueChanged.AddListener(this.onValueChanged);
		this.inviteButton.onClick.AddListener(this.onInvite);
		this.cancelButton.onClick.AddListener(this.onCancel);

		this.acceptChallengeButton.onClick.AddListener(this.onAccept);
		this.rejectChallengeButton.onClick.AddListener(this.onReject);


		InvokeRepeating("__challengeChecker", 1, 1);

	}

	void onValueChanged(float value) {
		this.meters = (int) value;
		this.meterText.text = this.meters.ToString() + " m";
	}

	void onInvite() {
		this.toggleChallengePanel(false);
		// extract info and invite
		Challenge challenge = new Challenge();
		challenge.challenged = lastSelectedPerson;
		challenge.challengedMetersRan = 0;
		challenge.challenger = PeopleInstanciator.myObject.me;
		challenge.challengerMetersRan = 0;
		challenge.meters = this.meters;
		challenge.status = ChallengeApiController.INVITED;
		
		cac.createChallenge(challenge, challengeResponse => {
			this.currentChallenge = challengeResponse;
		});
	}

	void onCancel() {
		this.toggleChallengePanel(false);
		// do nothing here!
	}

	override protected void OnMouseDown(Vector3 screenPosition) {
		PersonObject po = this.getPersonClicked(screenPosition);
		if (po != null) {
			// now i can challenge, but test if there is no challenge already running!
			if (this.currentChallenge != null && this.currentStatus == ChallengeApiController.ACCEPTED || this.currentStatus == ChallengeApiController.INVITED) {

			}
			else {
				Person p = lastSelectedPerson = po.me;
				this.setNameOfChallengedPerson(p);
				this.toggleChallengePanel(true);
			}
		}
	}

	void setNameOfChallengedPerson(Person other) {
		string identification = other.name != null && other.name != "" ? other.name : other.id;
		this.challengedName.text = CHALLENGE_TEXT.Replace("<NAME>", identification);
	}

	void toggleChallengePanel(bool shouldShow) {
		this.challengePanel.alpha = shouldShow ? 1f : 0f;
		this.challengePanel.interactable = shouldShow;
		this.challengePanel.blocksRaycasts = shouldShow;
	}

    PersonObject getPersonClicked(Vector3 clickPoint) {
        Ray ray = Camera.main.ScreenPointToRay(clickPoint);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, float.MaxValue)) {
            PersonObject buddy = hit.transform.gameObject.GetComponentInParent<PersonObject>();
            return buddy;                
        }

        return null;
    }

	void __updateRunningChallenge() {
		Person me = PeopleInstanciator.myObject.me;
		int lastDistanceRan = GeoLocationUtils.getDistanceBetweenPointsInMetersRound(me.lastLocation, me.location);
		
		// i was challenged
		if (this.currentChallenge.challenged.id == me.id) {
			this.currentChallenge.challengedMetersRan += lastDistanceRan;
		}
		// i created the challenge
		else {
			this.currentChallenge.challengerMetersRan += lastDistanceRan;
		}

		cac.update(this.currentChallenge, challengeUpdated => {			
			this.currentChallenge = challengeUpdated;
			// update meter running

			if (this.currentChallenge.status == ChallengeApiController.FINISHED) {
				this.defineStatus(this.currentChallenge);
				this.toggleMetersRan(false);
				CancelInvoke("__updateRunningChallenge");
			}
			else {
				this.updateMeterRunning(this.currentChallenge);
			}
		});
	}

	void defineStatus(Challenge challenge) {
		Person me = PeopleInstanciator.myObject.me;
		if (challenge.challenged.id == me.id) {
			if (challenge.challengedMetersRan > challenge.challengerMetersRan) {
				this.runStatus.text = "You win!";
			}
			else {
				this.runStatus.text = "You lose!";
			}
		}
		else {
			if (challenge.challengerMetersRan > challenge.challengedMetersRan) {
				this.runStatus.text = "You win!";
			}
			else {
				this.runStatus.text = "You lose!";
			}
		}
		
	}

	void updateMeterRunning(Challenge challenge) {

		toggleMetersRan(true);
		
		Person me = PeopleInstanciator.myObject.me;
		if (challenge.challenged.id == me.id) {
			metersCounter.text = METERS_RAN.Replace("<DIST>", challenge.challengedMetersRan.ToString());
		}
		// i created the challenge
		else {
			metersCounter.text = METERS_RAN.Replace("<DIST>", challenge.challengerMetersRan.ToString());
		}
	}

	void toggleMetersRan(bool shown) {
		CanvasGroup canvasGroup = metersCounter.GetComponent<CanvasGroup>();
		canvasGroup.alpha = shown ? 1f : 0f;
		canvasGroup.interactable = shown;
		canvasGroup.blocksRaycasts = shown;
	}

	void toggleAcceptanceMenu(bool shouldshow, Challenge challenge) {
		this.waitingForServerToResponde = true;
		acceptanceMenu.alpha = shouldshow ? 1f : 0f;
		acceptanceMenu.interactable = shouldshow;
		acceptanceMenu.blocksRaycasts = shouldshow;

		string identification = challenge.challenger != null && challenge.challenger.name != null ? challenge.challenger.name : challenge.challenger.id;
		acceptanceText.text = ACCEPTANCE_TEXT.Replace("<NAME>", identification).Replace("<DIST>", this.meters.ToString());
	}

	void onAccept() {
		this.toggleAcceptanceMenu(false, this.currentChallenge);
		
		this.waitingForServerToResponde = true;
		cac.acceptChallenge(this.currentChallenge, challengeUpdated => {
			this.waitingForServerToResponde = false;	
			this.currentChallenge = challengeUpdated;	
			InvokeRepeating("__updateRunningChallenge", 1f, 1f);
		});		
	}



	void onReject() {
		this.toggleAcceptanceMenu(false, this.currentChallenge);		
		this.waitingForServerToResponde = true;
		cac.rejectChallenge(this.currentChallenge, challengeUpdated => {	
			this.currentChallenge = challengeUpdated;
			this.waitingForServerToResponde = false;
		});
	}

	void __challengeChecker() {

		if (!waitingForServerToResponde) {

			cac.getMyChallenges(PeopleInstanciator.myObject.me, (challenge) => {
				
				this.currentStatus = challenge.status;
				if (this.currentStatus == ChallengeApiController.INVITED) {
					Debug.Log("UPDATING");
					// show accept menu
					this.currentChallenge = challenge;
					this.toggleAcceptanceMenu(true, this.currentChallenge);
					this.toggleChallengePanel(false);				
					this.currentChallenge.challengedMetersRan = this.currentChallenge.challengerMetersRan = 0;
					
				}
				else if (this.currentStatus == ChallengeApiController.ACCEPTED) {
					this.__updateRunningChallenge();
				}
			});
		}

	}
}
