using UnityEngine;
public class MouseCapture : MonoBehaviour {
    
    bool mouseMoving = false;
    Vector3 lastPosition = Vector3.zero;

    protected void StartCapture() {        
        InvokeRepeating("__GestureUpdater", 1, 1.0f/60.0f);        
    }

    protected void StopCapture() {
        CancelInvoke("__GestureUpdater");
    }

    void __GestureUpdater() {

        if (Input.GetMouseButtonDown(0)) {
            mouseMoving = true;
            this.OnMouseDown(Input.mousePosition);
        }
        
        if (Input.GetMouseButtonUp(0)) {
            mouseMoving = false;
            this.OnMouseUp(Input.mousePosition);
        }

        Vector3 deltaPosition = this.getMouseDelta();

        if (mouseMoving && deltaPosition.magnitude > 0.0f) {
            this.OnMouseMove(Input.mousePosition, deltaPosition);
        }
    }

    protected Vector3 getMouseDelta() {
        Vector3 deltaPosition = this.lastPosition - Input.mousePosition;
        this.lastPosition = Input.mousePosition;
        return deltaPosition;
    }

    virtual protected void OnMouseDown(Vector3 downPosition) {

    }

    virtual protected void OnMouseMove(Vector3 screenPosition, Vector3 deltaPosition) {
        // Debug.Log("MOUSE MOVING");
        // Debug.Log(deltaPosition);
    }

    virtual protected void OnMouseUp(Vector3 downPosition) {
        
    }
}