﻿using UnityEngine;
using System.Collections;

public class AutoRotator : MonoBehaviour {

	public Vector3 velocityVector = Vector3.zero;

	Vector3 yRot = new Vector3(0, 0, 0);
	Vector3 zRot = new Vector3(0, 0, 0);
	

	// Update is called once per frame
	void Update () {
		transform.localRotation = Quaternion.Euler (zRot);
		if (zRot.z > 360.0f || zRot.x > 360.0f || zRot.y > 360.0f) {
			zRot = Vector3.zero;
		} else {
			zRot -= velocityVector;
		}
	}
}
