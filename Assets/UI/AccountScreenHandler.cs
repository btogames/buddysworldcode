﻿using UnityEngine;
#if UNITY_5
using UnityEngine.SceneManagement;
#else
#endif
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;

public class AccountScreenHandler : MonoBehaviour {

	[SerializeField]
	private InputField emailField;
	[SerializeField]
	private InputField passwordField;
	[SerializeField]
	private Button continueButton;
	[SerializeField]
	private RectTransform loadingPanel;

	private PersonApiController pac = new PersonApiController ();

	void Awake() {

		continueButton.onClick.AddListener (onClickToLogin);
	}

	private string tryFindLoginJsonIntoDevice() {

		try {
			string loginJsonFilePath = Application.persistentDataPath + "/login.json";
			string jsonString = File.ReadAllText (loginJsonFilePath);
			return jsonString;
		}
		catch {
			return "";
		}
	}

	private void onClickToLogin() {
		Login login = new Login ();
		login.email = emailField.text;
		login.password = passwordField.text;

		if (login.email.Equals ("") || login.password.Equals ("")) {
			//TODO: implement error handler
		} 
		else {
			tryLoginWith (login);
		}
	}


	private void tryLoginWith(Login login) {
		Debug.Log("Try Login");
		pac.onLoginSuccess += onLoginSuccess;
		pac.onLoginFailed += (responseBody) => tryCreateAccountWith(login);
		pac.login (login.email, login.password);
	}

	private void tryCreateAccountWith(Login login) {
		Debug.Log("Try Creating Account");
		pac.onCreateAccountSuccess += (responseBody) => tryLoginWith(login);
		pac.onCreateAccountFail += onCreateAccountFail;
		pac.createAccount (login.email, login.password);
	}

	private void onLoginSuccess(AccessToken accessToken) {
		PersonApiController.currentAccessToken = accessToken;
#if UNITY_5
		SceneManager.LoadSceneAsync ("Game", LoadSceneMode.Single);
#else
		Application.LoadLevel("Game");
#endif
	}

	private void onLoginFailed (string responseBody) {
		
	}

	private void onCreateAccountSuccess(string responseBody) {

	}

	private void onCreateAccountFail() {

	}
	
}

[Serializable]
public class Login {
	public string email;
	public string password;
}
