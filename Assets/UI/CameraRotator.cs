using UnityEngine;
using System.Collections;

public class CameraRotator : MouseCapture
{

    private const float MAX_TIME = 5.0f;
    private float stopRotatingTime = 0.0f;
    private bool rotating = false;
    private Vector3 currentDelta;
    private StepsNavigator navigator;
    private bool released = false;
    private bool playingRotateBackAnimation = false;


    void Awake()
    {
        this.StartCapture();
        this.navigator = Camera.main.GetComponent<StepsNavigator>();
    }

    override protected void OnMouseMove(Vector3 screenPosition, Vector3 deltaPosition)
    {
        if (this.playingRotateBackAnimation)
            StopCoroutine("rotateBack");
        this.rotating = true;
        this.released = false;
        this.currentDelta += deltaPosition * Time.deltaTime;
    }

    override protected void OnMouseUp(Vector3 screenPosition)
    {
        this.stopRotatingTime = Time.time;
        this.released = true;
    }

    void Update()
    {

        if (this.rotating)
        {
            Camera main = Camera.main;
            GameObject me = PeopleInstanciator.myObject.gameObject;
            this.currentDelta -= this.currentDelta * Time.deltaTime * 3; // slowly decrement
            main.transform.RotateAround(me.transform.position, Vector3.up, -this.currentDelta.x);
        }

        if (this.released && this.stopRotatingTime + MAX_TIME <= Time.time) {
            this.released = this.rotating = false;
            // interpolate
			StartCoroutine(rotateBack());
        }
    }


    IEnumerator rotateBack() {

        this.playingRotateBackAnimation = true;
        float duration = 2f;
        float startTime = Time.time;
        float afterStepRun = startTime + duration;
        Camera main = Camera.main;
        GameObject me = PeopleInstanciator.myObject.gameObject;

        TransformEntity destinationEntity = this.navigator.getDestinationTransformEntify();

        Vector3 startpos = main.transform.localPosition;
        Vector3 endpos = destinationEntity.position;
		Quaternion startrotation = main.transform.localRotation;
		Quaternion endrotation = destinationEntity.rotation;

        while (Time.time <= afterStepRun) {

            float percentageRun = Mathfx.getPercentageOfTime(startTime, afterStepRun);
			float easeInOutPercentage = Mathfx.getEaseInOutPercentage(percentageRun);
            main.transform.localPosition = Vector3.Lerp(startpos, endpos, easeInOutPercentage);
            main.transform.localRotation = Quaternion.Slerp(startrotation, endrotation, easeInOutPercentage);
			
            yield return null;
        }

        main.transform.localPosition = endpos;
        main.transform.localRotation = endrotation;

        this.playingRotateBackAnimation = false;
        yield return null;
    }


}