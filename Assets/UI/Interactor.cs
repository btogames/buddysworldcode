using System;
using UnityEngine;

public class Interactor : MouseCapture {

    void Awake() {
        this.StartCapture();
    }

    override protected void OnMouseDown(Vector3 point) {
        
        GameObject other = this.getPersonClicked(point);
        if (other != null) {

            PersonObject po = other.GetComponent<PersonObject>();
            if (po != null)
                Debug.Log(po.me.toJson());
        }
    }

    GameObject getPersonClicked(Vector3 clickPoint) {
        Ray ray = Camera.main.ScreenPointToRay(clickPoint);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, float.MaxValue)) {
            BuddyInteractable buddy = hit.transform.gameObject.GetComponent<BuddyInteractable>();
            if (buddy != null)
                return buddy.gameObject;
        }

        return null;
    }
}