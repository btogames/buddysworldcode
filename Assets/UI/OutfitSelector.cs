using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class OutfitSelector : MonoBehaviour {


    [SerializeField]
    new private Text name;
    [SerializeField]
    private Text type;
    [SerializeField]
    private Button changeOutfitLeft;
    [SerializeField]
    private Button changeOutfitRight;
    [SerializeField]
    private Button changeTypeLeft;
    [SerializeField]
    private Button changeTypeRight;
    [SerializeField]
    private Button applyButton;
    [SerializeField]
    private GameObject visualizerParent;
    private CanvasGroup canvasGroup;

    private OutfitApiController oac = new OutfitApiController();

    private List<string> outfitAvailableTypes = new List<string>();
    private Outfit[] outfits = null;
    private List<Outfit> outfitsFromType = new List<Outfit>();
    private int outfitSelectionIndex = 0;
    private int typeSelectionIndex = 0;

    private StepsNavigator stepsNavigatorCam;
    private Outfit currentOutfit;



    void Awake() {
        canvasGroup = gameObject.AddOrGetComponent<CanvasGroup>();
        this.stepsNavigatorCam = Camera.main.GetComponent<StepsNavigator>();
        toggleVisibleAndInteractable();

        this.changeOutfitLeft.onClick.AddListener(this.onClickToChangeOutfitLeft);
        this.changeOutfitRight.onClick.AddListener(this.onClickToChangeOutfitRight);
        this.changeTypeLeft.onClick.AddListener(this.onClickToChangeOutfitTypeLeft);
        this.changeTypeRight.onClick.AddListener(this.onClickToChangeOutfitTypeRight);
        this.applyButton.onClick.AddListener(this.onApplyCurrentOutfit);
    }


    public void toogle(bool shouldShow) {
        if (shouldShow) this.show();
        else this.hide();
    }

    private void show() {
        outfitAvailableTypes.Clear();
        oac.getOutfits(processOutfits);
        this.stepsNavigatorCam.play();
    }

    private void hide() {
        this.toggleVisibleAndInteractable();
        this.stepsNavigatorCam.rewind();
    }

    private void processOutfits(Outfit[] outfits) {

        this.outfitAvailableTypes = this.extractAvailableTypes(outfits);
        this.outfits = outfits;

        if (this.outfitAvailableTypes.Count > 0) {
            this.setTypeOutfitsFrom(this.outfitAvailableTypes[0], this.outfits);
            toggleVisibleAndInteractable();
            this.onClickToChangeOutfitTypeLeft();
        }
    }

    private void setTypeOutfitsFrom(string type, Outfit[] outfits) {

        this.outfitsFromType.Clear();
        for(int i = 0; i < outfits.Length; i++) {
            Outfit outfit = outfits[i];
            if (outfit.type == type) {
                this.outfitsFromType.Add(outfit);
            }
        }
    }

    private List<string> extractAvailableTypes(Outfit[] outfits) {
        List<string> types = new List<string>();

        for(int i = 0; i < outfits.Length; i++) {
            string type = outfits[i].type;
            if (!types.Contains(type)) {
                types.Add(type);
            }
        }

        return types;
    }

    private void toggleVisibleAndInteractable() {
        canvasGroup.alpha = 1.0f - canvasGroup.alpha;
        canvasGroup.interactable = !canvasGroup.interactable;
        canvasGroup.blocksRaycasts = !canvasGroup.blocksRaycasts;
    }

    private void onClickToChangeOutfitLeft() {

        if (this.outfitsFromType.Count > 0) {
            Outfit outfit = this.outfitsFromType[outfitSelectionIndex % this.outfitsFromType.Count];
            this.name.text = outfit.name;
            if (outfitSelectionIndex == 0) {
                outfitSelectionIndex = this.outfitsFromType.Count - 1;
            }
            else {
                outfitSelectionIndex--;
            }

            this.currentOutfit = outfit;
        }
    }

    private void onClickToChangeOutfitRight() {

        if (this.outfitsFromType.Count > 0) {
            
            Outfit outfit = this.outfitsFromType[outfitSelectionIndex % this.outfitsFromType.Count];
            this.name.text = outfit.name;
            if (outfitSelectionIndex == this.outfitsFromType.Count - 1) {
                outfitSelectionIndex = 0;
            }
            else {
                outfitSelectionIndex++;
            }

            this.currentOutfit = outfit;
        }
    }

    private void onClickToChangeOutfitTypeLeft() {

        if (this.outfitAvailableTypes.Count > 0) {

            string type = this.outfitAvailableTypes[typeSelectionIndex % this.outfitAvailableTypes.Count];
            this.type.text = type;
            this.setTypeOutfitsFrom(type, this.outfits);
            if (this.typeSelectionIndex == 0) {
                this.typeSelectionIndex = this.outfitAvailableTypes.Count - 1;
            }
            else {
                this.typeSelectionIndex--;
            }

            this.onClickToChangeOutfitLeft();
        }
    }

    private void onClickToChangeOutfitTypeRight() {
        if (this.outfitAvailableTypes.Count > 0) {

            string type = this.outfitAvailableTypes[typeSelectionIndex % this.outfitAvailableTypes.Count];
            this.type.text = type;
            this.setTypeOutfitsFrom(type, this.outfits);
            if (this.typeSelectionIndex == this.outfitAvailableTypes.Count - 1) {
                this.typeSelectionIndex = 0;
            }
            else {
                this.typeSelectionIndex++;
            }
        }
    }

    private void onApplyCurrentOutfit() {
        
        this.setOutfit(this.currentOutfit);
    }

    private void setOutfit(Outfit outfit) {
        PersonObject po = PeopleInstanciator.myObject;
        Person pc = po.me;
        BodyOutfit body = po.GetComponent<BodyOutfit>();

        try {
            // set value to correct field
            FieldInfo info = pc.GetType().GetField(outfit.type);
            info.SetValue(pc, outfit.asset_id);
            body.setPersonConfig(pc);
        }
        catch(Exception e) {
            Debug.Log(e.Message);
        }
    }
}