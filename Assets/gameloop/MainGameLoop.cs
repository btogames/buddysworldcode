using System;
using UnityEngine;

public class MainGameLoop : MonoBehaviour {

    const float MINUTESINADAY = 24f * 60f; 

    void Awake() {
        GameObject.DontDestroyOnLoad(gameObject);
        UpdateSkiesColor();
        InvokeRepeating("UpdateSkiesColor", 1, 1);
    }

    void UpdateSkiesColor() {
        
        var date = DateTime.Now;
        float currentHour = date.Hour;
        float percentageOfTheDay = (currentHour * 60f + date.Minute) / MINUTESINADAY;
        float amountOfColor = 0f;

        if (percentageOfTheDay >= 0.25f && percentageOfTheDay <= 0.75f) {
            float relativePercentage = percentageOfTheDay - 0.5f;

            if (relativePercentage <= 0) {
                amountOfColor = Math.Abs(relativePercentage) / 0.5f;
            }
            else {
                amountOfColor = 1 - (Math.Abs(relativePercentage) / 0.5f);
            }
        }

        Color skyColor = new Color(amountOfColor, amountOfColor, amountOfColor, 1f);
        RenderSettings.skybox.SetColor("_Tint", skyColor);
        RenderSettings.ambientLight = skyColor; 
    }

    void Update() {
        
    }
}