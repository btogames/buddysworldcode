using System;
using Newtonsoft.Json;

[Serializable]
public class Challenge {

    public string accessToken;
    public string id;
    public int meters;
    public Person challenger;
    public int challengerMetersRan;
    public Person challenged;
    public int challengedMetersRan;
    public string status;

    static public Challenge create(string jsonData) {
        return JsonConvert.DeserializeObject<Challenge>(jsonData);
    }
    public string toJson() {
        JsonSerializerSettings settings = new JsonSerializerSettings();
		settings.MaxDepth = 3;
		settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
		
		return JsonConvert.SerializeObject(this, settings);
    }
}