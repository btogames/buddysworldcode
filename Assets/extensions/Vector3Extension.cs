using UnityEngine;

static public class Vector3Extension {

    static public bool nearWithPrecision(this Vector3 it, Vector3 other, float precision) {
        return Vector3.Distance(it, other) <= precision;
    }

    public static Vector3 Hermite(Vector3 t1, Vector3 v1, Vector3 v2, Vector3 t2, float s) {
        float s2 = s * s;
        float s3 = s2 * s;
        float p1 = ((2f * s3) - (3f * s2)) + 1f;
        float p2 = (-2f * s3) + (3f * s2);
        float p3 = (s3 - (2f * s2)) + s;
        float p4 = s3 - s2;
        Vector3 res = (v1 * p1) + (v2 * p2) + (t1 * p3) + (t2 * p4);
        return res;
    }

}