﻿using System;

static public class StringExtension
{
	static public byte[] ToByteArray(this string str) {
		return System.Text.Encoding.Default.GetBytes (str);
	}

	static public string joinPath(params string[] strings) {
		string pathDelimiter = "/";
		string ret = "";
		string cur = "";

		for(int i = 0; i < strings.Length; i++) {
			cur = strings[i];	
			ret += cur + (cur.Contains("/") ? "" : pathDelimiter);
		}

		return ret;
	} 
}

