﻿using UnityEngine;

public static class GameObjectExtension {


	static public T AddOrGetComponent<T>(this GameObject go) where T : Component {
		T component = go.GetComponent<T> ();
		return component == null ? go.AddComponent<T> () : component;
	}
}

