﻿using System;
using Newtonsoft.Json;
using UnityEngine;

static public class JsonArray
{

	static public T[] fromJsonArray<T>(string jsonArrayString) {
		Wrapper<T> wrapper = new Wrapper<T>();
		wrapper.Itens = new T[0];
		try {
			wrapper = JsonConvert.DeserializeObject<Wrapper<T>> (@"{""Itens"":" + jsonArrayString + "}");
		}
		catch (Exception e) {
			Debug.Log(e.Message);
		}
		
		return wrapper.Itens;
	}

	static public string toJsonArray<T>(T[] array) {
		Wrapper<T> wrapper = new Wrapper<T> ();
		wrapper.Itens = array;
		return JsonConvert.SerializeObject (wrapper);
	}

	[Serializable]
	private class Wrapper<T> {
		public T[] Itens;
	}
}

