using UnityEngine;

public static class TransformExtension {


	static public void ResetAll(this Transform t) {
        t.position = Vector3.zero;
        t.rotation = Quaternion.identity;
        t.localScale = Vector3.one;

        t.localPosition = Vector3.zero;
        t.localRotation = Quaternion.identity;
    }
}
