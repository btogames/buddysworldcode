﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OutfitLoader : MonoBehaviour
{
	public struct RequestAndParent {
		public GameObject parent;
		public ResourceRequest request;
		public string outfitpath;
	}

	private Dictionary<string, RequestAndParent> prefabCache = new Dictionary<string, RequestAndParent>();

	List<RequestAndParent> requests = new List<RequestAndParent> ();

	public void instantiateAndSetTo(string outfitPath, GameObject parent) {

		if (this.prefabCache.ContainsKey(outfitPath)) {
			RequestAndParent reqParent = this.prefabCache[outfitPath];
			reqParent.parent = parent;
			this.instantiateAndSetToParent(reqParent);
		}
		else {
			ResourceRequest outfit = Resources.LoadAsync (outfitPath);
			RequestAndParent reqParent = new RequestAndParent ();
			reqParent.parent = parent;
			reqParent.request = outfit;
			reqParent.outfitpath = outfitPath;

			if (parent != null) {
				requests.Add (reqParent);
			}
		}

	}


	void FixedUpdate() {

		for (int i = 0; i < requests.Count; i++) {
			RequestAndParent reqParent = requests [i];
			TryToInstantiateAndSetParentTo (reqParent);
		}
	}

	void TryToInstantiateAndSetParentTo(RequestAndParent reqParent) {

		ResourceRequest request = reqParent.request;
		GameObject parent = reqParent.parent;

		if (request.isDone) {
			
			if (request.asset) {
				prefabCache.Add(reqParent.outfitpath, reqParent);
				this.instantiateAndSetToParent(prefabCache[reqParent.outfitpath]);
			}

			requests.Remove (reqParent);
		}
	}

	void instantiateAndSetToParent(RequestAndParent reqParent) {
		ResourceRequest request = reqParent.request;
		GameObject parent = reqParent.parent;

		PivotChild child = parent.GetComponentInChildren<PivotChild>();
		if (child != null) GameObject.DestroyObject(child.gameObject);
		child = PivotChild.createNewWithParent(parent.transform);
		

		GameObject instance = GameObject.Instantiate (request.asset) as GameObject;
		// instance.transform.ResetAll();

		instance.transform.SetParent(child.transform, false);
		instance.transform.SetAsFirstSibling();
	}
}

