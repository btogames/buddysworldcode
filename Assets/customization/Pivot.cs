using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections.Generic;

public class Pivot : MonoBehaviour {

    [SerializeField]
    string pivotName = "";

    static Dictionary<string, Pivot> instances = new Dictionary<string, Pivot>();

    void Awake() {
        instances[pivotName] = this;
    }

    static public GameObject findPivotGameObjectByName(string pivotName) {
        return instances[pivotName].gameObject;
    }

    [ContextMenu("Set Pivot Name")]
    void setPivotNameWithGameObjectName() {
        this.pivotName = gameObject.name;
    }
}