using System;
using UnityEngine;

public class TorsoController : BaseController {

    GameObject torsoPivot;
    GameObject upperArmLeft;
    GameObject upperArmRight;
    GameObject downArmLeft;
    GameObject downArmRight;

    void Awake() {
        torsoPivot = Pivot.findPivotGameObjectByName("torso");
        upperArmLeft = Pivot.findPivotGameObjectByName("armLeft");
        upperArmRight = Pivot.findPivotGameObjectByName("armRight");
        downArmLeft = Pivot.findPivotGameObjectByName("armDownLeft");
        downArmRight = Pivot.findPivotGameObjectByName("armDownRight"); 
    }

    public override void setPersonConfig(Person personConfig) {
        
    }
}