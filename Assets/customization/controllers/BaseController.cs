using System.Collections;
using UnityEngine;

public abstract class BaseController : MonoBehaviour {

    protected OutfitLoader loader;
    public abstract void setPersonConfig(Person personConfig);
    public void setOutfitLoader(OutfitLoader loader) {
        this.loader = loader;
    }
}