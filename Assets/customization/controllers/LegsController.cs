using System;
using UnityEngine;

public class LegsController : BaseController
{
    GameObject legUpperLeftPivot;
    GameObject legUpperRightPivot;
    GameObject legDownLeftPivot;
    GameObject legDownRightPivot;
    GameObject waistPivot;

    void Awake() {
        waistPivot = Pivot.findPivotGameObjectByName(ControllersConts.Waist);
        legDownLeftPivot = Pivot.findPivotGameObjectByName(ControllersConts.DownLegLeft);
        legUpperRightPivot = Pivot.findPivotGameObjectByName(ControllersConts.UpperLegRight);
        legDownRightPivot = Pivot.findPivotGameObjectByName(ControllersConts.DownLegRight);
        legUpperLeftPivot = Pivot.findPivotGameObjectByName(ControllersConts.UpperLegLeft);
    }

    public override void setPersonConfig(Person personConfig) {
        if (personConfig.pants != "") {
            string sourcePath = (ControllersConts.LEGS_PATH + "/" + personConfig.pants + "/").Replace("//","/");
            loader.instantiateAndSetTo(sourcePath + ControllersConts.Waist, waistPivot);
            loader.instantiateAndSetTo(sourcePath + ControllersConts.UpperLegLeft, legUpperLeftPivot);
            loader.instantiateAndSetTo(sourcePath + ControllersConts.UpperLegRight, legUpperRightPivot);
            loader.instantiateAndSetTo(sourcePath + ControllersConts.DownLegLeft, legDownLeftPivot);
            loader.instantiateAndSetTo(sourcePath + ControllersConts.DownLegRight, legDownRightPivot);
        }
    }
}