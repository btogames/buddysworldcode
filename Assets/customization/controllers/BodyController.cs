using System;
using UnityEngine;


public class BodyController : BaseController {
    static GameObject bodyPrefab;
    const string CHARACTER_BODY_PATH = "character/body";
	const string IDLE = "idle";
	const string RUN = "run";
	
    GameObject body;

    void Awake() {
        instantiateBody();
    }
    public void playRunAnimation() {
		Animation found = this.body.GetComponentInChildren<Animation> ();
		if (found != null) {
			if (!found.IsPlaying (RUN)) {
				found.Play (RUN);
			}
		}
	}

    public void playIdleAnimation() {
		Animation found = this.body.GetComponentInChildren<Animation> ();
		if (found != null) {
			if (!found.IsPlaying (IDLE)) {
				found.Play (IDLE);
			}
		}
	}

    void instantiateBody() {
		bodyPrefab = tryLoadBodyPrefab ();
		instantiateBodyFrom (bodyPrefab);
	}

    GameObject tryLoadBodyPrefab() {		
		if (bodyPrefab == null)
			return Resources.Load<GameObject>(CHARACTER_BODY_PATH);

		return bodyPrefab;
	}

    void instantiateBodyFrom(GameObject prefab) {
		body = GameObject.Instantiate(prefab) as GameObject;
		body.transform.SetParent(transform, false);
	}


    public override void setPersonConfig(Person personConfig) {
        // throw new NotImplementedException();
    }
}