using UnityEngine;

public class SkinController : BaseController {
    
    Material skinMaterial;

    void Awake() {
        this.createsNewMaterialInstancesForSkinMaterial(gameObject);
    }

	public override void setPersonConfig(Person person) {
        Debug.Log(person);
        Debug.Log(skinMaterial);
        if (skinMaterial != null)
		    skinMaterial.color = person.skinColor;
	}

    void createsNewMaterialInstancesForSkinMaterial(GameObject body) {
		MeshRenderer[] renderers = body.GetComponentsInChildren<MeshRenderer>();

		for(int i = 0; i < renderers.Length; i++) {

			MeshRenderer r = renderers[i];

			if(r.sharedMaterials.Length > 0) {

				for(int j = 0; j < r.sharedMaterials.Length; j++) {

					Material m = r.sharedMaterials[j];
					if (m != null && m.name.Equals("skin")) {
                        Debug.Log("Found!");
						this.skinMaterial = r.sharedMaterials[j] = Resources.Load<Material>("skin"); 
					}
				}
			}
		}
	}
}