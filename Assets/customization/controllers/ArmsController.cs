using System;
using UnityEngine;

public class ArmsController : BaseController {
    
	const string ARMS_PATH = "customization/arms/";

    GameObject leftPivot;
    GameObject rightPivot;

	public override void setPersonConfig(Person personConfig) {
		
		string armsOutfit = personConfig.arms;
		string armsOutfitPath = ARMS_PATH + armsOutfit;
		loader.instantiateAndSetTo (armsOutfitPath, leftPivot);
		loader.instantiateAndSetTo (armsOutfitPath, rightPivot);
	}
}