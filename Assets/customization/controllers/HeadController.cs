using UnityEngine;

public class HeadController : BaseController {
    
    GameObject hairPivot;
    GameObject facePivot;


	void Awake() {
		hairPivot = Pivot.findPivotGameObjectByName("Head");
		facePivot = Pivot.findPivotGameObjectByName("Face");
	}

	public override void setPersonConfig(Person person) {
				
		if (person.hair != "") {
			string hairOutfitPath = ControllersConts.HEAD_PATH + person.hair;
			Debug.Log(hairOutfitPath);
			loader.instantiateAndSetTo (hairOutfitPath, hairPivot);
		}	

		if (person.face != "") {
			string faceOutfitPath = ControllersConts.FACE_PATH + person.face;
			loader.instantiateAndSetTo (faceOutfitPath, facePivot);
		}
	}
}