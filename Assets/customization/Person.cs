using System;
using UnityEngine;
using Newtonsoft.Json;

[Serializable]
public class Person {

	public string accessToken = "";
	public string id = "";
	public string name = "";
    public string hair = "";
    public string face = "";
    public string pants = "";
    public string shirt = "";
    public string arms = "";
    public string feet = "";
    public Color skinColor = Color.white;
    public Color hairColor = Color.white;
    public Location location = new Location();
	public Location lastLocation = new Location();
	public long metersRan = 0;

	static public Person createWithJson(string personConfigJson) {
		return JsonConvert.DeserializeObject<Person>(personConfigJson);
	}

	public string toJson() {
		JsonSerializerSettings settings = new JsonSerializerSettings();
		settings.MaxDepth = 3;
		settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
		
		return JsonConvert.SerializeObject(this, settings);
	}

	public bool isNear(Person person, float maxDistance = 0.2f) {
		return GeoLocationUtils.isNearMeMeters (location, person.location, maxDistance);
	}

	public bool Equals(Person other) {

		return	other.name == name &&
				other.hair == hair &&
				other.face == face &&
				other.pants == pants &&
				other.shirt == shirt &&
				other.arms == arms &&
				other.feet == feet &&
				other.skinColor == skinColor &&
				other.hairColor == hairColor;
		
	}
}

