public static class ControllersConts {
    public const string HEAD_PATH = "customization/head/";
    public const string FACE_PATH = "customization/face/";
    public const string TORSO_PATH = "customization/torso/";
    public const string LEGS_PATH = "customization/legs/";
    public const string ARMS_PATH = "customization/arms/";

    public const string Waist = "waist";
    public const string DownLegLeft = "legDownLeft";
    public const string DownLegRight = "legDownRight";
    public const string UpperLegLeft = "legLeft";
    public const string UpperLegRight = "legRight";

}