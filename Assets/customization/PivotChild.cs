using UnityEngine;


public class PivotChild : MonoBehaviour {

    static public PivotChild createNewWithParent(Transform parent) {
        GameObject go = new GameObject("pivot_child");
        // go.transform.ResetAll();
        go.transform.SetParent(parent, false);
        go.transform.SetAsFirstSibling(); 
        return go.AddOrGetComponent<PivotChild>();
    }
}