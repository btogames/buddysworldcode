using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PersonObject : MonoBehaviour {
	const string PERSON = "Person";	
	Queue<Vector3> positioningEnqueue = new Queue<Vector3>();
	Coroutine positioningCoroutine = null;
	BodyOutfit bodyOutfit;
	public Person me { get; private set; }
	
	static public PersonObject createFrom(Person personConfig) {
		
		GameObject pogo = new GameObject (PERSON);
        PersonObject po = pogo.AddComponent<PersonObject> ();
		po.setPersonConfig (personConfig);
		return po;
    }

	public void setPersonConfig(Person config) {

		if (me == null || !me.Equals(config)) {
			me = config;
			this.bodyOutfit.setPersonConfig(config);
		}
	}

	public void updatePosition(Vector3 newPosition) {
		
		if (!transform.position.nearWithPrecision(newPosition, 0.1f)) {
			positioningEnqueue.Enqueue(newPosition);
			if (positioningCoroutine == null)
				positioningCoroutine = StartCoroutine(interpolatingPositionViaQueue(0.0001f));
		}
	}

	public void updateRotationDirection(Vector3 fromDirection, Vector3 toDirection) {
		Vector3 lookDirection = toDirection - fromDirection;
		transform.rotation = Quaternion.Euler(lookDirection);
	}

	public void playRunAnimation() {
		this.bodyOutfit.getBody().playRunAnimation();
	}

	public void playIdleAnimation() {
		this.bodyOutfit.getBody().playIdleAnimation();
	}


	void Awake() {
		bodyOutfit = gameObject.AddOrGetComponent<BodyOutfit>();
	}
	IEnumerator interpolatingPositionViaQueue(float increment) {

		while(positioningEnqueue.Count > 0) {
			
			int currentCount = positioningEnqueue.Count;
			Vector3 destination = positioningEnqueue.Dequeue();

			// have (Queue amount) sec to interpolate until the end
			float startTime = Time.time;
			float currentTime = startTime;
			float endTime = startTime + 1 / currentCount;
			float approxTimeSlice = 0.0f;
			float currentPercentage = 0.0f;

			while(currentPercentage < 1.0f) {
				currentTime = Time.time;
				currentPercentage = currentTime / endTime;
				transform.position = Vector3.Lerp(transform.position, destination, currentPercentage);
				yield return null;
			}

			transform.position = destination;
			yield return null;
		}

		positioningCoroutine = null;
	}
}