using UnityEngine;
using System.Collections.Generic;

public class BodyOutfit : MonoBehaviour {
	List<BaseController> bodyControllers = new List<BaseController>();
    public void setPersonConfig(Person personConfig) {
		bodyControllers.ForEach( baseController => baseController.setPersonConfig(personConfig));
    }
    public BodyController getBody() {
        return gameObject.GetComponent<BodyController>();
    }
    void Awake() {
        attachBodyComponents();
		attachOutfitLoader();
    }

    void attachBodyComponents() {
		bodyControllers.Add(gameObject.AddOrGetComponent<BodyController>());
        bodyControllers.Add(gameObject.AddOrGetComponent<HeadController>());
		bodyControllers.Add(gameObject.AddOrGetComponent<ArmsController>());
		bodyControllers.Add(gameObject.AddOrGetComponent<TorsoController>());
		bodyControllers.Add(gameObject.AddOrGetComponent<LegsController>());
		bodyControllers.Add(gameObject.AddOrGetComponent<FeetController>());
		bodyControllers.Add(gameObject.AddOrGetComponent<SkinController>());
    }

	void attachOutfitLoader() {
		OutfitLoader outfit = gameObject.AddOrGetComponent<OutfitLoader>();
		bodyControllers.ForEach( baseController => baseController.setOutfitLoader(outfit));
	}
}