using System;
using Newtonsoft.Json;

[Serializable]
public class Outfit {

    public string id;
    public string name;
    public string type;
    public string asset_id;
    


    static public Outfit create(string jsonData) {
        return JsonConvert.DeserializeObject<Outfit>(jsonData);
    }

    public string toJson() {
        JsonSerializerSettings settings = new JsonSerializerSettings();
		settings.MaxDepth = 3;
		settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        return JsonConvert.SerializeObject(this, settings);
	}
}