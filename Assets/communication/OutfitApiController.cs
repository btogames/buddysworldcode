using UnityEngine;
using UnityEngine.Events;

public class OutfitApiController {
    
    private UnityAction<Outfit[]> getAllCallback = null;

    public void getOutfits(UnityAction<Outfit[]> callbackToGetOutifit) {
        getAllCallback = callbackToGetOutifit;
        Requester.getFromAndProcessWith(ServerConsts.OUTFIT_GET_ALL_URL, transformJsonToOutfitArray);
    }

    private void transformJsonToOutfitArray(string jsonData) {
        this.getAllCallback(JsonArray.fromJsonArray<Outfit>(jsonData));        
    }
}