﻿using System;
using Newtonsoft.Json;
using UnityEngine;

public class PersonApiController
{
	public static AccessToken currentAccessToken;
	public static Person currentConfig;

	public delegate void OnCreateAccountSuccess(string responseBody);
	public delegate void OnCreateAccountFail ();
	public delegate void OnLoginSuccess(AccessToken accessToken);
	public delegate void OnLoginFailed(string responseBody);
	public delegate void OnGETme(Person me);
	public delegate void OnPUTme(Person me);
	public delegate void OnRequestError(ServerError serverError);

	public event OnCreateAccountSuccess onCreateAccountSuccess;
	public event OnCreateAccountFail onCreateAccountFail;
	public event OnLoginSuccess onLoginSuccess;
	public event OnLoginFailed onLoginFailed;

	public event OnGETme onGETme;
	public event OnPUTme onPUTme;
	public event OnRequestError onRequestError;

	public void createAccount(string email, string password) {
		var jsonString = jsonLoginString (email, password);
		Requester.postJsonDataTo (ServerConsts.PERSON_CREATE_ACCOUNT_URL, jsonString, processCreateAccount);
	}

	public void login(string email, string password) {
		var jsonString = jsonLoginString (email, password);
		Requester.postJsonDataTo (ServerConsts.PERSON_LOGIN_URL, jsonString, processLogin);
	}

	public void getMe(AccessToken accessToken) {
		string accessTokenParam = "?access_token=" + accessToken.id;
		string finalUrl = ServerConsts.PERSON_ME_URL + accessTokenParam;
		Requester.getFromAndProcessWith (finalUrl, (responseBody) => { 
			
			Person me = Person.createWithJson (responseBody);
			if (me != null && !me.id.Equals("") && onGETme != null) {
				onGETme(me);
			}
			else if (onRequestError != null) {
				JsonSerializerSettings settings = new JsonSerializerSettings();
				settings.NullValueHandling = NullValueHandling.Include;
				ServerError error = JsonConvert.DeserializeObject<ServerError>(responseBody, settings);
				onRequestError(error);
			}
		});
	}

	public void updateMe(AccessToken accessToken, Person newMe) {

		Requester.postJsonDataTo (ServerConsts.PERSON_UPDATE_ME_URL, newMe.toJson(), accessToken, (responseBody) => { 
			JsonSerializerSettings settings = new JsonSerializerSettings();
			settings.NullValueHandling = NullValueHandling.Include;
			Person me = JsonConvert.DeserializeObject<Person> (responseBody, settings);
			try {
				if (me != null && !string.IsNullOrEmpty(me.id) && onPUTme != null) {				
					onPUTme(me);
				}
			}
			catch(Exception e) {
				if (onRequestError != null) {
					ServerError error = JsonConvert.DeserializeObject<ServerError>(responseBody);
					onRequestError(error);
				}
				else {
					Debug.LogError(e.Message);
				}
			}
 
		});
	}

	private string jsonLoginString(string email, string password) {
		string jsonString = @"{""email"":""#EMAILHOLDER#"",""password"":""#PASSWORDHOLDER#""}";
		return jsonString.Replace ("#EMAILHOLDER#", email).Replace ("#PASSWORDHOLDER#", password);
	}

	private void processCreateAccount(string responseBody) {
		if (onCreateAccountSuccess != null) {
			onCreateAccountSuccess (responseBody);
		}
	}

	private void processLogin(string responseBody) {

		try {
			var accessToken = JsonConvert.DeserializeObject<AccessToken> (responseBody);
			if (accessToken != null && !accessToken.id.Equals("") && onLoginSuccess != null) {
				onLoginSuccess (accessToken);
			} 
			else if (onLoginFailed != null) {
				onLoginFailed (responseBody);
			}
		}
		catch(Exception e) {
			onLoginFailed (responseBody);
			Debug.Log(responseBody);
			Debug.Log(e.Message);
		}
	}
}

[Serializable]
public class AccessToken {
	public string id;
	public int ttl;
	public string created;
	public string userId;
};

[Serializable]
public class ServerError {
	public Error error;
}

[Serializable]
public class Error {
	public string name;
	public string status;
	public string message;
}