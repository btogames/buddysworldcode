﻿public class ServerConsts {
	
	
	#if UNITY_EDITOR
	//public const string SERVER_URL = "http://localhost:8080";
	public const string SERVER_URL = "http://buddysworldapi-gilbertodev.rhcloud.com/";
	#else
	public const string SERVER_URL = "http://buddysworldapi-gilbertodev.rhcloud.com/";
	#endif

	public const string PERSON_API = "/api/people/";
	public const string PERSON_API_URL = ServerConsts.SERVER_URL + ServerConsts.PERSON_API;
	public const string PERSON_LOGIN_URL = ServerConsts.PERSON_API_URL + "login";
	public const string PERSON_CREATE_ACCOUNT_URL = ServerConsts.PERSON_API_URL;
	public const string PERSON_ME_URL = ServerConsts.PERSON_API_URL + "me";
	public const string PERSON_UPDATE_ME_URL = ServerConsts.PERSON_API_URL + "updateMe";

	public const string OUTFIT_API = "/api/outfits/";
	public const string OUTFIT_GET_ALL_URL = SERVER_URL + OUTFIT_API;


	public const string CHALLENGE_API = SERVER_URL + "/api/challenges/";
	public const string CHALLENGE_CREATE = CHALLENGE_API + "create";
	public const string CHALLENGE_UPDATE = CHALLENGE_API + "update";
	public const string CHALLENGE_FIND_CHALLENGES = CHALLENGE_API + "";

}

