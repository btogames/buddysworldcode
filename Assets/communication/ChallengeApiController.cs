using UnityEngine;
using UnityEngine.Events;
using Newtonsoft.Json;

public class ChallengeApiController {

    public const string INVITED = "INVITED";
    public const string ACCEPTED = "ACCEPTED";
    public const string FINISHED = "FINISHED";
    public const string REJECTED = "REJECTED";

    UnityAction<Challenge> getUpdatedChallenge;

    public void createChallenge(Challenge challenge, UnityAction<Challenge> onResponseHappenWithChallenge) {
        challenge.accessToken = PersonApiController.currentAccessToken.id;
        if (onResponseHappenWithChallenge != null) {
            string sendJsonString = challenge.toJson();
            Requester.postJsonDataTo(ServerConsts.CHALLENGE_CREATE, sendJsonString, (jsonString) => {

                Challenge challengeUpdated = JsonConvert.DeserializeObject<Challenge>(jsonString);
                if (challengeUpdated != null) onResponseHappenWithChallenge(challengeUpdated);                    
                else Debug.Log("Challenge: " + challengeUpdated);
            });
        } 
    }

    public void update(Challenge challenge, UnityAction<Challenge> onResponseHappen) {
        challenge.accessToken = PersonApiController.currentAccessToken.id;
        this.getUpdatedChallenge = onResponseHappen;
        Requester.postJsonDataTo(ServerConsts.CHALLENGE_UPDATE, challenge.toJson(), PersonApiController.currentAccessToken, this.transformJsonToChallengeUpdated);
    }

    public void acceptChallenge(Challenge challenge, UnityAction<Challenge> onResponseHappen) {
        challenge.accessToken = PersonApiController.currentAccessToken.id;
        challenge.status = ACCEPTED;
        Requester.postJsonDataTo(ServerConsts.CHALLENGE_UPDATE, challenge.toJson(), PersonApiController.currentAccessToken, (jsonString) => {            
            Challenge challengeUpdated = JsonConvert.DeserializeObject<Challenge>(jsonString);
            onResponseHappen(challengeUpdated);
        });
    }

    public void rejectChallenge(Challenge challenge, UnityAction<Challenge> onResponseHappen) {
        challenge.accessToken = PersonApiController.currentAccessToken.id;
        challenge.status = REJECTED;
        Requester.postJsonDataTo(ServerConsts.CHALLENGE_UPDATE, challenge.toJson(), PersonApiController.currentAccessToken, (jsonString) => {
            Debug.Log("REJECETEDDD: " + jsonString);
            Challenge challengeUpdated = JsonConvert.DeserializeObject<Challenge>(jsonString);
            onResponseHappen(challengeUpdated);
        });
    }

    public void getMyChallenges(Person me, UnityAction<Challenge> onResponseHappenWithChallenge) {

        if (onResponseHappenWithChallenge != null) {
            string URL = ServerConsts.CHALLENGE_FIND_CHALLENGES.Replace("<ID>", me.id);
            Requester.getFromAndProcessWith(URL, (jsonString) => {

                Challenge[] challengesUpdated = JsonConvert.DeserializeObject<Challenge[]>(jsonString);
                Challenge challenge = null;
                for(int i = 0; i < challengesUpdated.Length; i++) {
                    Challenge currentChallenge = challengesUpdated[i];
                    if (currentChallenge.challenged != null && currentChallenge.challenger != null && 
                    (currentChallenge.challenged.id == me.id)){// || currentChallenge.challenger.id == me.id)) {
                            challenge = currentChallenge;
                            break;
                    } 
                }
                if (challenge != null) onResponseHappenWithChallenge(challenge);                    
                else Debug.Log("Challenge: " + challenge);
            });
        }        
    }

    private void transformJsonToChallengeUpdated(string jsonData) {
        if (this.getUpdatedChallenge != null) {
            Challenge challengeUpdated = JsonConvert.DeserializeObject<Challenge>(jsonData);
            this.getUpdatedChallenge(challengeUpdated);
        }
    }
}