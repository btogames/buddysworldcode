﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
// using UnityEngine.Networking;

public class Requester : MonoBehaviour
{

	static GameObject requester;
	static Dictionary<string,string> jsonHeaders = new Dictionary<string, string> ();
	static Dictionary<string,string> jsonAndPutHeaders = new Dictionary<string, string> ();

	const string CONTENT_TYPE = "Content-Type";
	const string APPLICATION_JSON = "application/json";
	const string XHTTPMethodOverride = "X-HTTP-Method-Override";
	const string AUTHORIZATION = "Authorization";
	const string PUT = "PUT";
	const string POST = "POST";
	const string GET = "GET";
	const string DELETE = "DELETE";

	void Awake() {
		if (!jsonHeaders.ContainsKey(CONTENT_TYPE))
			jsonHeaders.Add (CONTENT_TYPE, APPLICATION_JSON);
	}

	static public void getFromAndProcessWith(string url, UnityAction<string> processResponse) {
		tryToInstantiateComponent ();
		Requester req = requester.AddOrGetComponent<Requester> ();
		req.StartCoroutine (handleRequest (GET, url, null, null, processResponse));
	}

	static public void postJsonDataTo(string url, string jsonString, AccessToken accessToken, UnityAction<string> processResponse) {
		tryToInstantiateComponent ();
		Requester req = requester.AddOrGetComponent<Requester> ();
		req.StartCoroutine (handlePostRequest (url, jsonString, accessToken, processResponse));
	}

	static public void postJsonDataTo(string url, string jsonString, UnityAction<string> processResponse) {
		tryToInstantiateComponent ();
		Requester req = requester.AddOrGetComponent<Requester> ();
		req.StartCoroutine (handlePostRequest (url, jsonString, null, processResponse));
	}

	static public void putJsonDataTo(string url, string jsonString, AccessToken accessToken, UnityAction<string> processResponse) {
		tryToInstantiateComponent ();
		Requester req = requester.AddOrGetComponent<Requester> ();
		req.StartCoroutine (handleRequest(PUT, url, jsonString, accessToken, processResponse));
	}

	static public void putJsonDataTo(string url, string jsonString, UnityAction<string> processResponse) {
		tryToInstantiateComponent ();
		Requester req = requester.AddOrGetComponent<Requester> ();
		req.StartCoroutine (handleRequest(PUT, url, jsonString, null, processResponse));
	}

	static IEnumerator handlePostRequest(string url, string jsonString, AccessToken accessToken, UnityAction<string> processResponse) {
		if (accessToken != null)
			jsonHeaders [AUTHORIZATION] = accessToken.id;
		
		using (var request = new WWW (url, jsonString.ToByteArray (), jsonHeaders)) {
			while (!request.isDone) {
				yield return null;
			}

			if (request.error != null) {
				processResponse (request.error);
			} 
			else {
				processResponse (request.text);
			}
		}
	}

	// static IEnumerator handleRequest(string method, string url, string jsonString, AccessToken accessToken, UnityAction<string> processResponse) {
	// 	using (var request = selectWebRequesterByMethod (method, url, jsonString)) {
	// 		request.SetRequestHeader (CONTENT_TYPE, APPLICATION_JSON);
	// 		if (accessToken != null) {
	// 			request.SetRequestHeader (AUTHORIZATION, accessToken.id);
	// 		}

	// 		yield return request.Send ();
	// 		string responseBody = DownloadHandlerBuffer.GetContent (request);
	// 		processResponse (responseBody);
	// 	}
	// }

	static IEnumerator handleRequest(string method, string url, string jsonString, AccessToken accessToken, UnityAction<string> processResponse) {

		Dictionary<string, string> headers = new Dictionary<string,string>();
		if(accessToken != null){
			Debug.Log("Access Token: " + accessToken.id);
			headers[AUTHORIZATION] = accessToken.id;
		}

		headers[CONTENT_TYPE] = APPLICATION_JSON;

		if (method.Equals(POST)) {
			
			using (var request = new WWW (url, jsonString.ToByteArray (), headers)) {
				while (!request.isDone) {
					yield return null;
				}

				if (request.error != null) {
					processResponse (request.error);
				} 
				else {
					processResponse (request.text);
				}
			}
		}
		else if (method.Equals(GET)) {
			using (var request = new WWW (url, null, headers)) {
				while (!request.isDone) {
					yield return null;
				}

				if (request.error != null) {
					processResponse (request.error);
				} 
				else {
					processResponse (request.text);
				}
			}
		}

		yield return null;
	}

	// static UnityWebRequest selectWebRequesterByMethod(string METHOD, string URL, string DATA) {
	// 	switch (METHOD) {
	// 	case GET:
	// 		return UnityWebRequest.Get (URL);
	// 		break;

	// 	case POST:
			
	// 		return UnityWebRequest.Post (URL, DATA);
	// 		break;

	// 	case PUT:
	// 		return UnityWebRequest.Put (URL, DATA);
	// 		break;

	// 	case DELETE:
	// 		return UnityWebRequest.Delete (URL);
	// 		break;

	// 	default:
	// 		return UnityWebRequest.Get (URL);
	// 		break;
	// 	}
	// }

	static void tryToInstantiateComponent() {
		if (requester == null) {
			requester = new GameObject ("Requester");
		}
	}
}

