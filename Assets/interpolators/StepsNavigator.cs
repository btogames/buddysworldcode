using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepsNavigator : MonoBehaviour {

    public float durationInSeconds = 5f;
    public GameObject lookPoint;
    bool isPlaying = false;
    bool isOnTheLastPosition = false;

    EntityStepPlanner planner;

    void Awake() {
        this.planner = gameObject.GetComponent<EntityStepPlanner>();
    }

    [ContextMenu("Play navigator")]
    public void play() {
        this.lookPoint = this.tryGetLookPoint();
        if (!this.isPlaying){
            StartCoroutine(animatingBezier(this.durationInSeconds));
            // StartCoroutine(animating(this.durationInSeconds/this.planner.getEntitySteps().Count, 1));
        }            
    }


    [ContextMenu("Rewind navigator")]
    public void rewind() {
        this.lookPoint = this.tryGetLookPoint();
        if (!this.isPlaying) {
            StartCoroutine(animatingBezierInverse(this.durationInSeconds));
            // StartCoroutine(animating(this.durationInSeconds/this.planner.getEntitySteps().Count, -1));
        }
    }

    public TransformEntity getDestinationTransformEntify() {
        List<TransformEntity> entities = this.planner.getEntitySteps();
        if (this.isOnTheLastPosition) {
            int plannerAmout = entities.Count;
            return entities[plannerAmout - 1];
        }
        else return entities[0];
    }

    GameObject tryGetLookPoint() {        
        return PeopleInstanciator.myObject.gameObject;
    }

    IEnumerator animatingBezier(float duration) {

        this.isPlaying = true;

        float startTime = Time.time;
        float afterStepRun = startTime + duration;

        Vector3 fromPosition = transform.localPosition;
        Vector3 fromScale = transform.localScale;
        Quaternion fromRotation = transform.localRotation;

        int plannerAmout = this.planner.getEntitySteps().Count;
        Vector3 toPosition = this.planner.getEntitySteps()[plannerAmout - 1].position;
        Vector3 toScale = this.planner.getEntitySteps()[plannerAmout - 1].scale;
        Quaternion toRotation = this.planner.getEntitySteps()[plannerAmout - 1].rotation;

        List<Vector3> points = new List<Vector3>();

        for(int i = 0; i < plannerAmout; i++) {
            Vector3 p = this.planner.getEntitySteps()[i].position;
            points.Add(p);
        }

        while(Time.time < afterStepRun) {

            float percentageRun = Mathfx.getPercentageOfTime(startTime, afterStepRun);
            
            transform.localPosition = GetBezierPoint(percentageRun, points, 0, plannerAmout);
            transform.localScale = Vector3.Lerp(fromScale, toScale, percentageRun);
            transform.localRotation = Quaternion.Slerp(fromRotation, toRotation, percentageRun);
            if (this.lookPoint != null)
                transform.LookAt(this.lookPoint.transform);

            yield return null;
        }

        transform.localPosition = toPosition;
        transform.localScale = toScale;
        transform.localRotation = toRotation;

        this.isPlaying = false;
        this.isOnTheLastPosition = true;

        yield return null;
    }

    IEnumerator animatingBezierInverse(float duration) {

        this.isPlaying = true;

        float startTime = Time.time;
        float afterStepRun = startTime + duration;

        Vector3 fromPosition = transform.localPosition;
        Vector3 fromScale = transform.localScale;
        Quaternion fromRotation = transform.localRotation;

        int plannerAmout = this.planner.getEntitySteps().Count;
        Vector3 toPosition = this.planner.getEntitySteps()[0].position;
        Vector3 toScale = this.planner.getEntitySteps()[0].scale;
        Quaternion toRotation = this.planner.getEntitySteps()[0].rotation;

        List<Vector3> points = new List<Vector3>();

        for(int i = 0; i < plannerAmout; i++) {
            Vector3 p = this.planner.getEntitySteps()[i].position;
            points.Add(p);
        }

        while(Time.time < afterStepRun) {

            float percentageRun = 1 - Mathfx.getPercentageOfTime(startTime, afterStepRun);
            
            transform.localPosition = GetBezierPoint(percentageRun, points, 0, plannerAmout);
            transform.localScale = Vector3.Lerp(fromScale, toScale, percentageRun);
            transform.localRotation = Quaternion.Slerp(fromRotation, toRotation, percentageRun);
            if (this.lookPoint != null)
                transform.LookAt(this.lookPoint.transform);

            yield return null;
        }

        transform.localPosition = toPosition;
        transform.localScale = toScale;
        transform.localRotation = toRotation;

        this.isPlaying = false;
        this.isOnTheLastPosition = false;

        yield return null;
    }


    Vector3 GetBezierPoint(float t, List<Vector3> controlPoints, int index, int count) {
        if (count == 1)
            return controlPoints[index];
        var P0 = GetBezierPoint(t, controlPoints, index, count - 1);
        var P1 = GetBezierPoint(t, controlPoints, index + 1, count - 1);
        return new Vector3((1 - t) * P0.x + t * P1.x, (1 - t) * P0.y + t * P1.y, (1 - t) * P0.z + t * P1.z);
    }
}