using System;
using System.Collections.Generic;
using UnityEngine;

public class EntityStepPlanner : MonoBehaviour {

    [SerializeField]
    private int currentPlanIndex = 0;
    
    [SerializeField]
    private List<TransformEntity> entitySteps = new List<TransformEntity>();

    [ContextMenu("Set Current Config")]
    public void setCurrentConfig() {

        if (this.currentPlanIndex < this.countConfigSet()) {
            TransformEntity entityStep = new TransformEntity(transform.localPosition, transform.localScale, transform.localRotation);
            this.entitySteps[this.currentPlanIndex] = entityStep;
        }
    }

    [ContextMenu("Add Current Config")]
    public void addCurrentConfig() {
        
        TransformEntity entityStep = new TransformEntity(transform.localPosition, transform.localScale, transform.localRotation);
        this.entitySteps.Add(entityStep);        
        this.currentPlanIndex += 1;
    }

    [ContextMenu("Remove config at current index")]
    public void removeCurrentIndex() {
        if (this.currentPlanIndex < this.countConfigSet()) {
            this.entitySteps.RemoveAt(this.currentPlanIndex);
        }
    }

    [ContextMenu("Next Config")]
    public void nextConfig() {
        if (this.currentPlanIndex < this.countConfigSet() - 1){
            this.currentPlanIndex++;
        }            
    }

    [ContextMenu("Previous Config")]
    public void previousConfig() {
        if (this.currentPlanIndex > 0) {
            this.currentPlanIndex--;
        }
    }

    [ContextMenu("Apply config at current index")]
    public void applyConfigAtCurrentIndex() {
        TransformEntity entityStep = this.entitySteps[this.currentPlanIndex];
        transform.localPosition = entityStep.position;
        transform.localScale = entityStep.scale;
        transform.localRotation = entityStep.rotation;
    }

    public List<TransformEntity> getEntitySteps() {
        return this.entitySteps;
    }

    public int countConfigSet() {
        return this.entitySteps.Count;
    }

}