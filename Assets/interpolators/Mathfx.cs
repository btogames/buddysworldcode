using UnityEngine;
public static class Mathfx {

    public static float getPercentageOfTime(float start, float end) {

        float elapsedTime = Time.time - start;
        float finalDiffeTime = end - start;
        return elapsedTime / finalDiffeTime;
    }

    public static float getEaseInPercentage(float percentage) {
        return Mathf.SmoothStep(0.0f, 1.0f, percentage);
    }

    public static float getEaseInOutPercentage(float percentage) {
        return Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, percentage));
    }
}