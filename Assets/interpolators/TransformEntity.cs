using System;
using UnityEngine;

[Serializable]
public class TransformEntity {

    [SerializeField]
    public Vector3 position;
    [SerializeField]
    public Vector3 scale;
    [SerializeField]
    public Quaternion rotation;


    public TransformEntity(Vector3 position, Vector3 scale, Quaternion rotation) {
        this.position = position;
        this.scale = scale;
        this.rotation = rotation;
    }
}