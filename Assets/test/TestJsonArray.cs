﻿using UnityEngine;
using System.Collections;

public class TestJsonArray : MonoBehaviour
{
	[ContextMenu("Parse person config array")]
	void parsePeopleJsonConfigArray() {
		string jsonPersonList = @"[{""id"":1,""name"":""Gilberto"",""hair"":""courier"",""face"":""rayban"",""pants"":""black"",""shirt"":""white"",""arms"":""null"",""feet"":""nike"",""skinColor"":{""r"":0.5,""g"":0.6,""b"":0.7},""hairColor"":{""r"":0.4,""g"":0.3,""b"":0.2},""location"":{""latitude"":-45.123,""longitude"":-25.456},""lastLocation"":{""latitude"":-45.100,""longitude"":-25.499}},{""id"":""1b29371912c9e7bce79"",""name"":""Gilberto"",""hair"":""courier"",""face"":""rayban"",""pants"":""black"",""shirt"":""white"",""arms"":""null"",""feet"":""nike"",""skinColor"":{""r"":0.5,""g"":0.6,""b"":0.7},""hairColor"":{""r"":0.4,""g"":0.3,""b"":0.2},""location"":{""latitude"":-45.123,""longitude"":-25.456},""lastLocation"":{""latitude"":-45.100,""longitude"":-25.499}},{""id"":""1b12bcf2fbbacc12903"",""name"":""Gilberto"",""hair"":""courier"",""face"":""rayban"",""pants"":""black"",""shirt"":""white"",""arms"":""null"",""feet"":""nike"",""skinColor"":{""r"":0.5,""g"":0.6,""b"":0.7},""hairColor"":{""r"":0.4,""g"":0.3,""b"":0.2},""location"":{""latitude"":-45.123,""longitude"":-25.456},""lastLocation"":{""latitude"":-45.100,""longitude"":-25.499}}]";


		print (jsonPersonList);
		Person[] people = JsonArray.fromJsonArray<Person> (jsonPersonList);


		print (people);
		print (people[0].id);
		print (people[1].id);
		print (people[2].id);
	}

	[ContextMenu("Parse data got from server")]
	void parseDataFromServer() {
		Requester.getFromAndProcessWith (ServerConsts.PERSON_API_URL, processPeopleAsJsonArray);
	}

	void processPeopleAsJsonArray(string jsonString) {
		Person[] people = JsonArray.fromJsonArray<Person> (jsonString);

		foreach (Person person in people) {
			print (person.toJson());
		}
	}
}

