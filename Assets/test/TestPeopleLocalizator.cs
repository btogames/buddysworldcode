﻿using UnityEngine;
using System.Collections;

public class TestPeopleLocalizator : MonoBehaviour
{
	PeopleLocalizator pl;

	[ContextMenu("Test people locatlizator")]
	void testPeopleLocalizator() {
		pl = PeopleLocalizator.create ();
		Location location = new Location {
			lat = -29.923400f,
			lng = -51.039613f
		};
		pl.maxDistance = 100.0f;
		pl.getWhoIsAroundMyLocationAndSetProcessingFunction (location, printPeopleJsonData);
	}

	void printPeopleJsonData(Person[] people) {
		foreach(Person person in people) {
			print (person.toJson ());
		}
	}
}

