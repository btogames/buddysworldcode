using UnityEngine;
using System.Collections;

public class TestGetAllOutfits: MonoBehaviour {


    [ContextMenu("Test get all outfits from server")]
    void getAllOutfitsFromServer() {
        OutfitApiController oac = new OutfitApiController();
        oac.getOutfits(this.processAllOutfits);
    }

    void processAllOutfits(Outfit[] outfits) {
        foreach(Outfit outfit in outfits) {
            Debug.Log(outfit.toJson());
        }
    }
}