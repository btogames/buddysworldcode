﻿using UnityEngine;
using Newtonsoft.Json;
using System.Collections;

/*
	public string id;
	public string name;
    public string hair;
    public string face;
    public string pants;
    public string shirt;
    public string arms;
    public string feet;
    public Color skinColor;
    public Color hairColor;

    public LocationInfo position;
*/

public class TestPersonConfig : MonoBehaviour
{
	Person config;

	bool isReady = false;

	IEnumerator Start() {
		
		if (!Input.location.isEnabledByUser) {
			Debug.Log ("Location Service is not Enabled");
			yield return null;
		}

		Input.location.Start (5, 5);

		int maxWait = 20;
		if (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds (1);
			maxWait--;
		}

		if (maxWait < 1) {
			print ("Timeout");
			yield break;
		}


		if (Input.location.status == LocationServiceStatus.Failed || Input.location.status == LocationServiceStatus.Stopped) {
			print ("Failed");
			yield break;
		} 
		else {
			isReady = true;
		}
	}

	[ContextMenu("Test person from json")]
	void fromJsonToPersonConfig() {
		string jsonPerson = @"{
			""id"" : ""1b12bcf2fb2928bacfed"",
			""name"" : ""Gilberto"",
			""hair"" : ""courier"",
			""face"" : ""rayban"",
			""pants"" : ""black"",
			""shirt"" : ""white"",
			""arms"" : ""null"",
			""feet"" : ""nike"",
			""skinColor"" : {
				""r"" : 0.5,
				""g"" : 0.6,
				""b"" : 0.7
			},
			""hairColor"" : {
				""r"" : 0.4,
				""g"" : 0.3,
				""b"" : 0.2			
			},
			""location"" : {
				""latitude"" : -45.123,
				""longitude"" : -25.456
			},
			""lastLocation"" : {
				""latitude"" : -45.100,
				""longitude"" : -25.499
			}
		}";

		LocationInfo location = Input.location.lastData;
		print("JSON LOCATION: " + JsonConvert.SerializeObject (location));

		Debug.Log (jsonPerson);
		config = Person.createWithJson (jsonPerson);

		Debug.Log (config);
	}

	[ContextMenu("Test json from person")]
	void fromPersonConfigToJson() {
		string jsonPerson = config.toJson ();
		Debug.Log (jsonPerson);
	}
}

