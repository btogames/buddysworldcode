using UnityEngine;
using System.Collections;

public class TestGPSDistanceCalc : MonoBehaviour
{
    [SerializeField]
    Location p1;
    [SerializeField]
    Location p2;

    [ContextMenu("Calculate Distance")]
    void calculateDistance() {
        Debug.Log(GeoLocationUtils.getDistanceBetweenPointsInMeters(p1, p2) + " m");
    }

    [ContextMenu("Calculate Distance Round")]
    void calculateDistanceRound() {
        Debug.Log(GeoLocationUtils.getDistanceBetweenPointsInMetersRound(p1, p2) + " m approx.");
    }
}