﻿using UnityEngine;
using System.Collections;

public class TestGetDataFromServer : MonoBehaviour
{
	[ContextMenu("Get www.google.com.br data")]
	void getGoogleData() {
		Requester.getFromAndProcessWith("https://www.google.com.br/", print);
	}

	[ContextMenu("Send data to checkbeer")]
	void trySendPostData() {
		string url = "http://138.68.57.87/api/admins/login";
		string jsonString = @"{
			""email"" : ""default@checkbeer.com"",
			""password"" : ""default""
		}";

		Requester.postJsonDataTo (url, jsonString, print);
	}

}

