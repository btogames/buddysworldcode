﻿using UnityEngine;
using Newtonsoft.Json;

public class TestPersonApiController: MonoBehaviour
{
	string email = "";
	string password = "test";
	Person me;
	PersonApiController pac = new PersonApiController ();
	AccessToken accessToken;

	[ContextMenu("Test create account")]
	void testCreateAccount() {
		

		pac.onCreateAccountSuccess += (string responseBody) => print (responseBody);
		email = Random.value.ToString () + "@gmail.com";
		pac.createAccount (email, password);
	}

	[ContextMenu("Test login")]
	void testLogin() {
		
		pac.onLoginSuccess += (accessToken) => {this.accessToken = accessToken; print(JsonConvert.SerializeObject (accessToken));};
		pac.onLoginFailed += (responseBody) => print(responseBody);

		pac.login (email, password);
	}

	[ContextMenu("Test get my config")]
	void testGetMyConfig() {
		pac.onRequestError += (error) => print(error.error.message);
		pac.onGETme += (me) => {
			Debug.Log("HERE!" + me);
			this.me = me;			
			print (me.toJson ());
		};

		pac.getMe (accessToken);
	}

	[ContextMenu("Test update my config")]
	void testUpdateMyConfig() {
		pac.onPUTme += attribNewMe;
		me.name = "Gilberto Ribeiro";
		me.skinColor = Color.cyan;
		me.hairColor = new Color (.5f, .6f, .7f);

		pac.updateMe (accessToken, me);
	}

	void attribNewMe(Person newMe) {
		me = newMe;
		print (me.toJson ());
	}
}

